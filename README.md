# COVID 19 modeling for India

## Summary

[PDF](https://gitlab.com/simm/covid19/india.secir/raw/master/static/summary.pdf)

## Article

[PDF](https://gitlab.com/simm/covid19/india.secir/raw/master/static/article.pdf)

## Presentation of the modeling work

[PDF](https://gitlab.com/simm/covid19/india.secir/raw/master/static/modeling_work.pdf)


